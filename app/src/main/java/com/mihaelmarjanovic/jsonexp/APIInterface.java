package com.mihaelmarjanovic.jsonexp;


import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface APIInterface {
    @GET("usd.json")
    Call<List<rates>> getUsers();
}
