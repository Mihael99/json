package com.mihaelmarjanovic.jsonexp;

public class Address {
    private String street;
    private String suite;

    public String getStreet(){
        return street;
    }

    public String getSuite(){
        return suite;
    }
}
