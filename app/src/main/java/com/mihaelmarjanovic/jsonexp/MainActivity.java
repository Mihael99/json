package com.mihaelmarjanovic.jsonexp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.TimeUnit;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private TextView tvUserInfo;
    private Call<List<rates>> apiCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvUserInfo = findViewById(R.id.tvUserInfo);
        Log.i("Before ApiCall", "Arrived");
        setUpApiCall();
    }

    private void setUpApiCall() {
        apiCall = NetworkUtils.getApiInterface().getUsers();
        Log.i("After getUsers()", "Arrived");
        apiCall.enqueue(new Callback<List<rates>>() {
            @Override
            public void onResponse(Call<List<rates>> call, Response<List<rates>> response) {
                if(response.isSuccessful() && response.body() != null){
                    Log.i("Successful response", "Arrived");
                    showUsers(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<rates>> call, Throwable t) {
                Log.i("Failed response", "Arrived");
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showUsers(List<rates> data) {
        StringBuilder stringBuilder = new StringBuilder();
        for(rates tempRates : data) {
            stringBuilder.append(tempRates.getRate()).append("\n");
        }
        tvUserInfo.setText(stringBuilder.toString());
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(apiCall!=null)
            apiCall.cancel();
    }
}